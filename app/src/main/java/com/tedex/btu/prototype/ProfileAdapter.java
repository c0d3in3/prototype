package com.tedex.btu.prototype;

import android.app.Activity;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.MyViewHolder> {
    private List<Image> imageList;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView userPostImage;
        public FirebaseStorage storage;


        public MyViewHolder(View view) {
            super(view);
            userPostImage = (ImageView) view.findViewById(R.id.userPostImage);
            storage = FirebaseStorage.getInstance();

        }
    }


    public ProfileAdapter(List<Image> imageList) {
        this.imageList = imageList;
    }

    @Override
    public ProfileAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.profile_list_item, parent, false);
        return new ProfileAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ProfileAdapter.MyViewHolder holder, int position) {
        if(getItemCount() != 0){
            Image image = imageList.get(position);
            StorageReference storageRef = holder.storage.getReference();
            StorageReference pathReference = storageRef.child(image.getImageUrl());
            pathReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Picasso.get().load(uri).into(holder.userPostImage);
                }

            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }
}
