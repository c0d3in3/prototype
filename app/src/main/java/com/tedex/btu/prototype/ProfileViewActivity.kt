package com.tedex.btu.prototype

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Camera
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_profile_view.*
import java.util.ArrayList

class ProfileViewActivity : AppCompatActivity() {

    private val imageList = ArrayList<Image>()
    private lateinit var recyclerView: RecyclerView
    private lateinit var mAdapter: ProfileAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_view)

        recyclerView = findViewById<View>(R.id.recycleView_profileImages) as RecyclerView

        mAdapter = ProfileAdapter(imageList)
        val mLayoutManager = GridLayoutManager(applicationContext, 3)
        recyclerView.layoutManager = mLayoutManager
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = mAdapter

        blurAll()
        getDataFromDatabase()
        init()

    }

    fun init() {
        profileButton.setOnClickListener {
            val auth = FirebaseAuth.getInstance()
            val user = auth.currentUser
            val intent = getIntent()
            val uid = intent.getStringExtra("uid")
            val last_uid = intent.getStringExtra("last_uid")
            if (user != null) {
                val intent = Intent(this, ProfileViewActivity::class.java)
                if(uid != null) {
                    intent.putExtra("last_uid", uid)
                    intent.removeExtra("uid")
                }
                if(last_uid != null){
                    intent.putExtra("last_uid", last_uid)
                }
                startActivity(intent)
                finish()
            }
        }
        homeButton.setOnClickListener {
            val intent = Intent(this, NewsFeedActivity::class.java)
            startActivity(intent)
            finish()
        }

        userImage.setOnClickListener {
            val intent = Intent(this, CameraActivity::class.java)
            startActivity(intent)
            intent.putExtra("updateProfile", "true")
        }
    }

    @SuppressLint("SetTextI18n")
    fun getDataFromDatabase() {
        val db = FirebaseFirestore.getInstance()
        val auth = FirebaseAuth.getInstance()
        val user = auth.currentUser
        val intent = getIntent()
        val uid = intent.getStringExtra("uid")
        if (user != null) {
            if (uid != null) {
                db.collection("users").document(uid)
                        .get()
                        .addOnSuccessListener { result ->
                            Log.d("GetUsersData", uid + " => " + result.data)
                            userButton.text = result.getString("username")
                            userTextView.text = result.getString("name") + " " + result.getString("surname")
                            if (result.getString("followers")!!.isEmpty()) {
                                followersTextView.text = "0 Followers"
                            }
                            else {
                                val followers = result.getString("followers")!!.split(",")
                                followersTextView.text = followers.size.toString() + " Followers"
                            }
                            if (result.getString("following")!!.isEmpty()) {
                                followingTextView.text = "0 Following"
                            } else {
                                val following = result.getString("followers")!!.split(",")
                                followingTextView.text = following.size.toString() + " Following"
                            }
                            val storage = FirebaseStorage.getInstance()
                            val storageRef = storage.getReferenceFromUrl("gs://btuprototype.appspot.com")
                            val pathReference = storageRef.child(result.getString("image")!!)

                            pathReference.downloadUrl.addOnSuccessListener { uri ->
                                // Got the download URL for 'users/me/profile.png'
                                // Pass it to Picasso to download, show in ImageView and caching
                                Picasso.get().load(uri.toString()).into(userImage)
                            }.addOnFailureListener {
                                // Handle any errors
                            }
                        }
                        .addOnFailureListener { exception ->
                            Log.w("GetUsersData", "Error getting documents.", exception)
                        }

                db.collection("images")
                        .get()
                        .addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                for (document in task.result!!) {
                                    if (document.getString("image_author") == uid) {
                                        Log.d("GetUserImages", document.id + " => " + document.data)
                                        val image = Image(document.getString("image_author"), document.getString("image_authorUsername"), document.getString("image_url"), document.getString("image_url"), document.getString("image_likes"), document.getString("image_text"), document.getString("document_id"), uid, document.getString("image_date"))
                                        imageList.add(image)
                                        mAdapter.notifyDataSetChanged()
                                        unblurAll()
                                    }
                                }
                            } else {
                                Log.d("GetUserImages", "Error getting documents.", task.exception)
                            }
                        }
            } else {
                db.collection("users").document(user.uid)
                        .get()
                        .addOnSuccessListener { result ->
                            Log.d("GetUsersData", user.uid + " => " + result.data)
                            userButton.text = result.getString("username")
                            userTextView.text = result.getString("name") + " " + result.getString("surname")
                            if (result.getString("followers")!!.isEmpty()) {
                                followersTextView.text = "0 Followers"
                            }
                            else {
                                val followers = result.getString("followers")!!.split(",")
                                followersTextView.text = followers.size.toString() + " Followers"
                            }
                            if (result.getString("following")!!.isEmpty()) {
                                followingTextView.text = "0 Following"
                            } else {
                                val following = result.getString("followers")!!.split(",")
                                followingTextView.text = following.size.toString() + " Following"
                            }
                            val storage = FirebaseStorage.getInstance()
                            val storageRef = storage.getReferenceFromUrl("gs://btuprototype.appspot.com")
                            val pathReference = storageRef.child(result.getString("image")!!)

                            pathReference.downloadUrl.addOnSuccessListener { uri ->
                                // Got the download URL for 'users/me/profile.png'
                                // Pass it to Picasso to download, show in ImageView and caching
                                Picasso.get().load(uri.toString()).into(userImage)
                            }.addOnFailureListener {
                                // Handle any errors
                            }
                        }
                        .addOnFailureListener { exception ->
                            Log.w("GetUsersData", "Error getting documents.", exception)
                        }

                db.collection("images")
                        .get()
                        .addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                for (document in task.result!!) {
                                    if (document.getString("image_author") == user.uid) {
                                        Log.d("GetUserImages", document.id + " => " + document.data)
                                        val image = Image(document.getString("image_author"), document.getString("image_authorUsername"), document.getString("image_url"), document.getString("image_url"), document.getString("image_likes"), document.getString("image_text"), document.getString("document_id"), uid, document.getString("image_date"))
                                        imageList.add(image)
                                        mAdapter.notifyDataSetChanged()
                                        unblurAll()
                                    }
                                }
                            } else {
                                Log.d("GetUserImages", "Error getting documents.", task.exception)
                            }
                        }
            }

        }
    }

    override fun onBackPressed() {
        val auth = FirebaseAuth.getInstance()
        val user = auth.currentUser
        val intent = getIntent()
        val uid = intent.getStringExtra("uid")
        val last_uid = intent.getStringExtra("last_uid")
        if (uid == null && last_uid == null) {
            val intent = Intent(this, NewsFeedActivity::class.java)
            startActivity(intent)
            finish()
        }
        if(uid != null){
            val intent = Intent(this, NewsFeedActivity::class.java)
            startActivity(intent)
            finish()
        }
        if(uid == null && last_uid != null){
            val intent = Intent(this, ProfileViewActivity::class.java)
            intent.putExtra("uid", last_uid)
            intent.removeExtra("last_uid")
            startActivity(intent)
            finish()
        }
    }

    private fun blurAll(){
        content_layout.alpha = 0.0f
    }

    private fun unblurAll(){
        content_layout.alpha = 1.0f
    }
}
