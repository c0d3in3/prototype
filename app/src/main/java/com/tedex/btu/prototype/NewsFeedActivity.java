package com.tedex.btu.prototype;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import static android.util.Log.d;

public class NewsFeedActivity extends AppCompatActivity implements View.OnClickListener {

    private List<Image> imageList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ImageAdapter mAdapter;
    private Button exitButton;
    private Button profileButton;
    private Button cameraButton;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_feed);

        recyclerView = (RecyclerView) findViewById(R.id.recycleView_Images);

        mAdapter = new ImageAdapter(imageList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mAdapter);

        getDataFromDatabase();
        prepareImageData();

        init();
    }

    public void onClick(View v) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        final FirebaseUser user = auth.getCurrentUser();
        switch(v.getId()) {
            case R.id.profileButton:
                intent = new Intent(this, ProfileViewActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.exitButton:
                FirebaseAuth.getInstance().signOut();
                intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.cameraButton:
                intent = new Intent(this, CameraActivity.class);
                startActivity(intent);

        }
    }


    private void init() {
        exitButton = (Button) findViewById(R.id.exitButton);
        exitButton.setOnClickListener(this);
        profileButton = (Button) findViewById(R.id.profileButton);
        profileButton.setOnClickListener(this);
        cameraButton = (Button) findViewById(R.id.cameraButton);
        cameraButton.setOnClickListener(this);
    }


    private void prepareImageData() {
    }

    private void getDataFromDatabase(){
        FirebaseAuth auth = FirebaseAuth.getInstance();
        final FirebaseUser user = auth.getCurrentUser();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("images")
            .get()
            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        int number = 0;
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            d("getDb", document.getId() + " => " + document.getData());
                            Image image = new Image(document.getString("image_author"), document.getString("image_authorUsername"), document.getString("image_url"), document.getString("image_url"), document.getString("image_likes"), document.getString("image_text"), document.getString("document_id"), user.getUid(), document.getString("image_date"));
                            imageList.add(image);

                        }
                        mAdapter.notifyDataSetChanged();

                    } else {
                        d("getDb", "Error getting documents.", task.getException());
                    }
                }
            });
    }
}