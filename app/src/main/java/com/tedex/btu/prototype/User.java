package com.tedex.btu.prototype;

public class User {
    private String Username;
    private String Name;
    private String Surname;
    private Long Number;
    private String Followers;
    private String Following;

    public User() {
    }

    public User(String username, String name, String surname, Long number, String followers, String following) {
        Username = username;
        Name = name;
        Surname = surname;
        Number = number;
        Followers = followers;
        Following = following;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public Long getNumber() {
        return Number;
    }

    public void setNumber(Long number) {
        Number = number;
    }

    public String getFollowers() {
        return Followers;
    }

    public void setFollowers(String followers) {
        Followers = followers;
    }

    public String getFollowing() {
        return Following;
    }

    public void setFollowing(String following) {
        Following = following;
    }
}

