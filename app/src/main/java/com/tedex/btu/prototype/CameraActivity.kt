package com.tedex.btu.prototype

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_camera.*
import org.joda.time.DateTime
import java.io.ByteArrayOutputStream
import java.net.URI
import java.util.*

class CameraActivity : AppCompatActivity() {

    val REQUEST_IMAGE_CAPTURE = 1
    lateinit var imageBitmap: Bitmap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)
        dispatchTakePictureIntent()
        init()
    }

    private fun dispatchTakePictureIntent(){
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also {takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also{
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK){
            if(data != null){
                imageBitmap = data.extras.get("data") as Bitmap
                cameraImageView.setImageBitmap(imageBitmap)
            }
        }
        else{
            val intent = Intent(this, NewsFeedActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun init() {
        profileButton.setOnClickListener {
            val auth = FirebaseAuth.getInstance()
            val user = auth.currentUser
            if (user != null) {
                val intent = Intent(this, ProfileViewActivity::class.java)
                intent.putExtra("uid", user.uid)
                startActivity(intent)
            }
        }
        homeButton.setOnClickListener {
            val intent = Intent(this, NewsFeedActivity::class.java)
            startActivity(intent)
            finish()
        }

        takeAgainButton.setOnClickListener{
            dispatchTakePictureIntent()
        }
        uploadImageButton.setOnClickListener {
            if(titleEditText.text.toString().isEmpty()){
                KCustomToast.infoToast(this, "Please enter image title!", KCustomToast.GRAVITY_TOP)
            }
            else{
                val db = FirebaseFirestore.getInstance()
                val auth = FirebaseAuth.getInstance()
                val imageData = HashMap<String, Any?>()
                val currentUser = auth.currentUser
                if (currentUser != null) {
                    db.collection("users").document(currentUser.uid)
                            .get()
                            .addOnSuccessListener { result ->
                                if(result != null){
                                    Log.d("GetUsersData", currentUser.uid + " => " + result.data)
                                    val user = result.getString("username")
                                    imageData["image_authorUsername"] = user
                                    imageData["image_date"] = DateTime().toString()
                                    imageData["image_comments"] = 0
                                    imageData["image_id"] = 0
                                    imageData["image_likes"] = ""
                                    imageData["image_author"] = currentUser.uid
                                    val ref:DocumentReference = db.collection("images").document()
                                    imageData["image_url"] = "images/" + ref.id
                                    imageData["document_id"] = ref.id
                                    imageData["image_text"] = titleEditText.text.toString()
                                    db.collection("images").document(ref.id)
                                            .set(imageData)
                                            .addOnSuccessListener {
                                                Log.d("tag", "success")
                                            }
                                            .addOnFailureListener { e ->
                                                Log.d("InsertData", "Error adding document", e)
                                            }
                                    uploadImage(ref.id)

                                    db.collection("users").document(currentUser.uid).update("image","images/" + ref.id)
                                    .addOnSuccessListener { Log.d("updateProfileImage", "DocumentSnapshot successfully updated!") }
                                    .addOnFailureListener { e -> Log.w("updateProfileImage", "Error updating document", e) }

                                    val intent = Intent(this, NewsFeedActivity::class.java)
                                    startActivity(intent)
                                    finish()
                                }
                            }
                            .addOnFailureListener { exception ->
                                Log.w("GetUsersData", "Error getting documents.", exception)
                            }

                }
            }
            }
    }

    override fun onBackPressed(){
        val intent = Intent(this, NewsFeedActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun uploadImage(name:String){
        val storage = FirebaseStorage.getInstance()
        val imageRef = storage.reference.child("images/$name")
        cameraImageView.isDrawingCacheEnabled = true
        cameraImageView.buildDrawingCache()
        val bitmap = (cameraImageView.drawable as BitmapDrawable).bitmap
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()

        var uploadTask = imageRef.putBytes(data)
        uploadTask.addOnFailureListener {

        }.addOnSuccessListener {
            Log.d("url", imageRef.downloadUrl.toString())
        }
    }
}

