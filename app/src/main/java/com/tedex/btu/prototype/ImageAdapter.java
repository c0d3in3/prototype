package com.tedex.btu.prototype;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import org.joda.time.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.util.Log.d;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.MyViewHolder> {

    private List<Image> imagesList;

    private Intent intent;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public Button author;
        public ImageView image;
        public ImageView authorImage;
        public TextView imageText;
        public TextView imageLikes;
        public Button likeButton;
        public FirebaseAuth mAuth;
        public FirebaseUser mUser;
        public String mUid;
        public Boolean bool;
        public FirebaseFirestore db;
        public FirebaseStorage storage;
        public TextView imageDate;
        public String authorId;

        public String imageLikesString;
        public String documentId;

        public MyViewHolder(View view) {
            super(view);
            author = (Button) view.findViewById(R.id.authorName);
            authorImage = (ImageView) view.findViewById(R.id.authorProfileImage);
            image = (ImageView) view.findViewById(R.id.authorPostImage);
            imageText = (TextView) view.findViewById(R.id.postText);
            imageLikes = (TextView) view.findViewById(R.id.postLikes);
            likeButton = (Button) view.findViewById(R.id.likeButton);
            imageDate = (TextView) view.findViewById(R.id.postDate);
            author.setOnClickListener(this);
            likeButton.setOnClickListener(this);
            mAuth = FirebaseAuth.getInstance();
            mUser = mAuth.getCurrentUser();
            mUid = mUser.getUid();
            bool = false;
            db = FirebaseFirestore.getInstance();
            storage = FirebaseStorage.getInstance();
        }

        public void onClick(View v) {
            if(v.getId() == author.getId()){
                FirebaseAuth auth = FirebaseAuth.getInstance();
                final FirebaseUser user = auth.getCurrentUser();
                intent = new Intent(v.getContext(), ProfileViewActivity.class);
                intent.putExtra("uid", authorId);
                v.getContext().startActivity(intent);
                ((Activity)v.getContext()).finish();
            }
            if (v.getId() == likeButton.getId()) {
                if (!imageLikesString.contains(mUid)) {
                    likeButton.setBackgroundResource(R.drawable.red_heart);

                    DocumentReference documentReference = db.collection("images").document(documentId);
                    imageLikesString = imageLikesString + mUid + ",";
                    documentReference
                            .update("image_likes", imageLikesString)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    d("UpdateData", "DocumentSnapshot successfully updated!" + documentId);
                                    String[] image_likes = imageLikesString.split(",");
                                    if (imageLikesString.isEmpty()) {
                                        String imageLikesUpdated = "<b>0 likes</b>";
                                        imageLikes.setText(Html.fromHtml(imageLikesUpdated));
                                    } else {
                                        String imageLikesUpdated = "<b>" + image_likes.length + " likes</b>";
                                        imageLikes.setText(Html.fromHtml(imageLikesUpdated));
                                    }
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    d("UpdateData", "Error updating document", e);
                                }
                            });
                } else {
                    likeButton.setBackgroundResource(R.drawable.transparent_heart);
                    DocumentReference docRef = db.collection("images").document(documentId);

                    imageLikesString = imageLikesString.replace(mUid + ",", "");

                    docRef.update("image_likes", imageLikesString).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            String[] image_likes = imageLikesString.split(",");
                            if (imageLikesString.isEmpty()) {
                                String imageLikesUpdated = "<b>0 likes</b>";
                                imageLikes.setText(Html.fromHtml(imageLikesUpdated));
                            }
                            else {
                                String imageLikesUpdated = "<b>" + image_likes.length + " likes</b>";
                                imageLikes.setText(Html.fromHtml(imageLikesUpdated));
                            }
                        }
                        // ...
                        // ...

                    });
                }
            }
        }
    }


    public ImageAdapter(List<Image> imagesList) {
        this.imagesList = imagesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_list_item, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Image image = imagesList.get(position);
        holder.authorId = image.getAuthor();
        holder.author.setText(image.getAuthorUsername());
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl("gs://btuprototype.appspot.com");
        StorageReference pathReference = storageRef.child(image.getImageUrl());

        pathReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                // Pass it to Picasso to download, show in ImageView and caching
                Picasso.get().load(uri.toString()).into(holder.image);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("users").document(image.getUserId()).get()
        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    d("getDb", document.getId() + " => " + document.getData());
                    FirebaseStorage storage = FirebaseStorage.getInstance();
                    StorageReference storageRef = storage.getReferenceFromUrl("gs://btuprototype.appspot.com");
                    StorageReference pathReference = storageRef.child(document.getString("image"));
                    pathReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        // Got the download URL for 'users/me/profile.png'
                        // Pass it to Picasso to download, show in ImageView and caching
                        Picasso.get().load(uri.toString()).into(holder.authorImage);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle any errors
                    }
                });
                } else {
                    d("getDb", "Error getting documents.", task.getException());
                }
            }
        });
        holder.image.setAdjustViewBounds(true);
        holder.image.setScaleType(ImageView.ScaleType.CENTER_CROP);
        String imageText = "<b>" + image.getAuthorUsername() + "</b> " + image.getImageText();
        holder.imageText.setText(Html.fromHtml(imageText));
        String[] image_likes = image.getImageLikes().split(",");

        if (image.getImageLikes().isEmpty()) {
            String imageLikes = "<b>0 likes</b>";
            holder.imageLikes.setText(Html.fromHtml(imageLikes));
        }
        else{
            String imageLikes = "<b>" + image_likes.length + " likes</b>";
            holder.imageLikes.setText(Html.fromHtml(imageLikes));
        }

        holder.mUid = image.getUserId();
        if(image.getImageLikes().contains(holder.mUid)){
            holder.likeButton.setBackgroundResource(R.drawable.red_heart);
        }
        else {
            holder.likeButton.setBackgroundResource(R.drawable.transparent_heart);
        }
        holder.documentId = image.getImageDocumentId();
        holder.imageLikesString = image.getImageLikes();

        DateTime dateTime = new DateTime();
        int days = Days.daysBetween(DateTime.parse(image.getImageDate()), dateTime).getDays();
        if(days < 1){
            String daysString = "TODAY";
            holder.imageDate.setText(daysString);
        }
        else{
            String daysString = days + " DAYS AGO";
            holder.imageDate.setText(daysString);
        }

    }

    @Override
    public int getItemCount() {
        return imagesList.size();
    }

    public long getItemId(int position) {
        return position;
    }
}