package com.tedex.btu.prototype

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*
import com.google.firebase.auth.FirebaseUser



class LoginActivity : AppCompatActivity() {


    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        auth = FirebaseAuth.getInstance()
        init()
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        if (currentUser != null) {
            // User is signed in
            val intent = Intent(this, NewsFeedActivity::class.java)
            startActivity(intent)
            finish()
        } else {
            // User is signed out
            d("Login", "onAuthStateChanged:signed_out")
        }
    }

    private fun init(){
        logInButton.setOnClickListener {
            if (!passwordEditText.text.toString().isEmpty() && !emailEditText.text.toString().isEmpty()) {
                auth.signInWithEmailAndPassword(emailEditText.text.toString(), passwordEditText.text.toString())
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            d("Sign In", "signInWithEmail:success")
                            val user = auth.currentUser
                            val intent = Intent(this, NewsFeedActivity::class.java)
                            startActivity(intent)
                            finish()
                        } else {
                            // If sign in fails, display a message to the user.
                            d("Sign In", "signInWithEmail:failure", task.exception)
                            KCustomToast.infoToast(this, "Email or password is incorrect!", KCustomToast.GRAVITY_TOP)
                        }
                    }
            }
            else{
                KCustomToast.infoToast(this, "Email or password field is empty!", KCustomToast.GRAVITY_TOP)
            }
        }
        signUpButton.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }
    }
}
