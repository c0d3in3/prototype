package com.tedex.btu.prototype

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_sign_up_info.*

class SignUpInfoActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_info)
        auth = FirebaseAuth.getInstance()
        insertData()
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
    }

    private fun insertData(){
        val db = FirebaseFirestore.getInstance()
        finalSignUpButton.setOnClickListener {
            val user = HashMap<String, Any>()
            val currentUser = auth.currentUser
            var numberString : String = numberEditText.text.toString()
            var number : Long = numberString.toLong()
            user["username"] = nickEditText.text.toString()
            user["name"] = nameEditText.text.toString()
            user["surname"] = surnameEditText.text.toString()
            user["number"] = number
            user["followers"] = ""
            user["following"] = ""
            if (!nickEditText.text.toString().isEmpty() && !nameEditText.text.toString().isEmpty() && !surnameEditText.text.toString().isEmpty()
                    && !numberEditText.toString().isEmpty()) {
                if(currentUser != null){
                    db.collection("users").document(currentUser.uid)
                            .set(user)
                            .addOnSuccessListener {
                                val intent = Intent(this, NewsFeedActivity::class.java)
                                startActivity(intent)
                                finish()
                            }
                            .addOnFailureListener { e ->
                                d("InsertData", "Error adding document", e)
                            }
                }
            }
            else{
                KCustomToast.infoToast(this, "Please fill all the fields!", KCustomToast.GRAVITY_TOP)
            }
        }
    }
}
