package com.tedex.btu.prototype

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        auth = FirebaseAuth.getInstance()
        init()
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        if (currentUser != null) {
            // User is signed in
            val intent = Intent(this, NewsFeedActivity::class.java)
            startActivity(intent)
            finish()
        } else {
            // User is signed out
            d("Sign Up", "onAuthStateChanged:signed_out")
        }
    }

    private fun init(){
        signUpButton.setOnClickListener{
            if(!emailEditText.text.toString().isEmpty() && !passwordEditText.text.toString().isEmpty()){
                auth.createUserWithEmailAndPassword(emailEditText.text.toString(), passwordEditText.text.toString())
                        .addOnCompleteListener(this) { task ->
                            if (task.isSuccessful) {
                                // Sign in success, update UI with the signed-in user's information
                                d("SignUp", "createUserWithEmail:success")
                                val user = auth.currentUser
                                val intent = Intent(this, SignUpInfoActivity::class.java)
                                startActivity(intent)
                                finish()
                            } else {
                                // If sign in fails, display a message to the user.
                                d("SignUp", "createUserWithEmail:failure", task.exception)
                                KCustomToast.infoToast(this, "Authentication failed!", KCustomToast.GRAVITY_TOP)
                            }

                            // ...
                        }
            }
            else{
                KCustomToast.infoToast(this, "Please fill all the fields!", KCustomToast.GRAVITY_TOP)
            }

        }
        logInButton.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}
