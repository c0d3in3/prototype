package com.tedex.btu.prototype;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.auth.User;

public class Image {
    private String Author;
    private String AuthorUsername;
    private String ImageUrl;
    private String AuthorImage;
    private String ImageLikes;
    private String ImageText;
    private String ImageDocumentId;
    private String UserId;
    private String ImageDate;


    public Image(){
    }

    public Image(String author, String authorUsername, String imageUrl, String authorImage, String imageLikes, String imageText, String imageDocumentId, String userId, String imageDate) {
        Author = author;
        AuthorUsername = authorUsername;
        ImageUrl = imageUrl;
        AuthorImage = authorImage;
        ImageLikes = imageLikes;
        ImageText = imageText;
        ImageDocumentId = imageDocumentId;
        UserId = userId;
        ImageDate = imageDate;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getAuthorUsername() {
        return AuthorUsername;
    }

    public void setAuthorUsername(String authorUsername) {
        AuthorUsername = authorUsername;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getAuthorImage() {
        return AuthorImage;
    }

    public void setAuthorImage(String authorImage) {
        AuthorImage = authorImage;
    }

    public String getImageLikes() {
        return ImageLikes;
    }

    public void setImageLikes(String imageLikes) {
        ImageLikes = imageLikes;
    }

    public String getImageText() {
        return ImageText;
    }

    public void setImageText(String imageText) {
        ImageText = imageText;
    }

    public String getImageDocumentId() {
        return ImageDocumentId;
    }

    public void setImageDocumentId(String imageDocumentId) {
        ImageDocumentId = imageDocumentId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getImageDate() {
        return ImageDate;
    }

    public void setImageDate(String imageDate) {
        ImageDate = imageDate;
    }
}
